package CircleDemo;

/**
 * Created by Игорь on 30.06.2017.
 */
class CircleDemo {
    private double Radius;
    private double Ference;
    private double Area;

    public void setRadius(double r) {
        Radius = r;
        Ference = 2 * Math.PI * Radius;
        Area = Math.PI * Math.pow(Radius,2);
    }
    public void setFerence(double f) {
        Ference = f;
        Radius = Ference / (2 * Math.PI);
        Area = Math.PI * Math.pow(Radius,2);
    }
    public void setArea(double a) {
        Area = a;
        Radius = Math.sqrt(Area/Math.PI);
        Ference = 2 * Math.PI * Radius;
    }
    public double getRadius(){
        return Radius;
    }
    public double getFerence(){
        return Ference;
    }
    public double getArea(){
        return Area;
    }
}

class EarthAndRope {
    CircleDemo Earth = new CircleDemo();
    CircleDemo RopePlusPlus = new CircleDemo();

    public double SizeOfGap() {
        Earth.setRadius(6378.1);
        RopePlusPlus.setFerence(Earth.getFerence() + 0.001);
        return RopePlusPlus.getRadius() - Earth.getRadius();
    }
}

class Pool {
    CircleDemo CPool = new CircleDemo();
    CircleDemo PoolWithRoad = new CircleDemo();

    public double CostOfConcrete() {
        CPool.setRadius(3);
        PoolWithRoad.setRadius(3 + 1);
        return ((PoolWithRoad.getArea() - CPool.getArea()) * 1000);
    }
    public double CostOfFence() {
        return (PoolWithRoad.getFerence() * 2000);
    }
}

public class ProjectCircleDemo {
    public static void main(String args[]) {
        System.out.println("Земля и верёвка.");
        EarthAndRope earthAndRope = new EarthAndRope();
        System.out.println("Величина зазора: " + earthAndRope.SizeOfGap());
        System.out.println();
        System.out.println("Бассейн.");
        Pool coolpool = new Pool();
        System.out.println("Стоимость материалов бетонной дорожки: " + coolpool.CostOfConcrete());
        System.out.println("Стоимость материалов ограды: " + coolpool.CostOfFence());
    }
}

