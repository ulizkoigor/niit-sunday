package ParseNumRange;

/**
 * Created by ulizko on 28.06.2017.
 */
public class ParseNumRange {
    public static void main(String args[]) {
        String a, b;
        int i, j;

        System.out.println("Начальная строка:");
        for(i=0; i < args[0].length(); i++) {
            if(((args[0].charAt(i) >= '0') && (args[0].charAt(i) <= '9')) | (args[0].charAt(i) == ',') | (args[0].charAt(i) == '-'))
                System.out.print(args[0].charAt(i));
            else {
                System.out.println();
                System.out.println("Недопустимый символ - '" + args[0].charAt(i) + "'. Аргумент должен состоять из цифр или знаков ',' и '-' ");
                System.exit(0);
            }
        }
        a = "";
        b = "";
        System.out.println();
        System.out.println("Конечная строка:");
        for(i=0; i < args[0].length(); ) {
            if((args[0].charAt(i) >= '0') && (args[0].charAt(i) <= '9')) {
                a += args[0].charAt(i);
                i++;
                if(i == args[0].length()) System.out.print(Integer.parseInt(a) + " ");
            }
            else if(args[0].charAt(i) == ',') {
                System.out.print(Integer.parseInt(a) + " ");
                a = "";
                i++;
            }
            else if (args[0].charAt(i) == '-') {
                i++;
                do {
                    b += args[0].charAt(i);
                    i++;
                } while (i != args[0].length() && (args[0].charAt(i) != ','));
                for (j = Integer.parseInt(a); j <= Integer.parseInt(b); j++)
                    System.out.print(j + " ");
                a = "";
                b = "";
                if(i == args[0].length()) break;
                else i++;
            }
        }
    }
}
