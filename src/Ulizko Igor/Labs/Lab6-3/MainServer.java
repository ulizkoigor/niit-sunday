import javafx.application.Application;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.layout.FlowPane;
import javafx.stage.Stage;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;



public class MainServer extends Application {
    static ArrayList<Event> events = new ArrayList<Event>();

    public static void main(String[] args) {
        launch(args);
    }

    public void init() throws IOException {
        JSONParser parser = new JSONParser();
        try {
            JSONObject jsonObject = (JSONObject) parser.parse(new FileReader("events.json"));
            JSONArray jsonEvents = (JSONArray) jsonObject.get("events");
            for (Object jsonEvent: jsonEvents) {
                String id = (String)(((JSONObject) jsonEvent).get("id"));
                long time = (long) (((JSONObject) jsonEvent).get("time"));
                String event = (String)(((JSONObject) jsonEvent).get("event"));

                events.add(new Event(id, (int)time, event));
            }
        } catch (ParseException ex) {
            System.out.println(ex.getMessage());
        } catch(IOException ex) {
            System.out.println(ex.getMessage());
        }
    }


    public void start(Stage myStage) throws IOException {
        new Server();

        myStage.setTitle("The server is running");

        FlowPane rootNode = new FlowPane(10,10);

        rootNode.setAlignment(Pos.CENTER);

        Scene myScene = new Scene(rootNode,300,300);

        myStage.setScene(myScene);

         Label label = new Label("The server is running");

        rootNode.getChildren().add(label);

        myStage.show();
    }

    public void stop() throws IOException {
        Server.serverSocket.close();
    }

}

class Server extends Thread {
    static ServerSocket serverSocket;
    static int countConnection = 0;

    Server() throws IOException {
        serverSocket = new ServerSocket (1234);
        start();
    }

    public void run() {
        while (true) {
            try {
                Socket clientSocket = serverSocket.accept();
                countConnection++;
                System.out.println(countConnection);
                new ServerOne(clientSocket);

            } catch (IOException exc) {
                exc.printStackTrace();
                System.exit(-1);
            }
        }
    }
}

class ServerOne extends Thread {
    private Socket socket;
    private BufferedReader in;
    private PrintWriter out;

    public ServerOne(Socket socket) throws IOException {
        this.socket = socket;
        in = new BufferedReader(new InputStreamReader(this.socket.getInputStream()));
        out = new PrintWriter(this.socket.getOutputStream(), true);
        start();
    }

    public void run() {
        String cmd;

         while (true) {
             try {
                 if ((cmd = in.readLine()).length() != 0) {
                     if (cmd.equals("stop")) {
                         socket.close();
                         System.out.println(--Server.countConnection);
                         break;
                     }
                     for (Event event : MainServer.events) {
                         if (cmd.equals(event.id)) {
                             out.println(event.id);
                             out.println(event.time);
                             out.println(event.event);
                             break;
                         }
                     }
                 }

             } catch (IOException e) {
                 e.printStackTrace();
             }
        }
    }
}

class Event {
    String id;
    int time;
    String event;

    Event (String id, int time, String event) {
        this.id = id;
        this.time = time;
        this.event = event;
    }
}