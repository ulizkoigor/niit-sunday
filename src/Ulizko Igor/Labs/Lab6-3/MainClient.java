import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.layout.FlowPane;
import javafx.stage.Stage;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

import static javafx.application.Application.launch;

public class MainClient extends Application {
    Label label;
    Socket server;
    BufferedReader input;
    PrintWriter output;

    public static void main(String[] args)  {
        launch(args);
    }

    public void init() {
        try {
            server = new Socket("127.0.0.1", 1234);

            input = new BufferedReader(new InputStreamReader(server.getInputStream()));
            output = new PrintWriter(server.getOutputStream(), true);

        } catch (IOException exc) {
            System.out.println((LocalTime.now().format(DateTimeFormatter.ofPattern("HH:mm:ss")) + " Connection error at 172.0.0.1 port 1234"));
            System.exit(-1);
        }
    }

    public void start(Stage myStage) {
        myStage.setTitle("Client connected");

        FlowPane rootNode = new FlowPane(10,10);

        rootNode.setAlignment(Pos.CENTER);

        Scene myScene = new Scene(rootNode,400,500);

        myStage.setScene(myScene);

        label = new Label("Enter the ID and click");

        ScrollPane scrollPane = new ScrollPane(label);
        scrollPane.setPrefViewportWidth(300);
        scrollPane.setPrefViewportHeight(400);

        TextField textField = new TextField();

        Button button = new Button("The event request");

        button.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                String result;
                if (textField.getText().length() == 0)
                    return;
                output.println(textField.getText());
                try {
                    result = input.readLine();
                    label.setText(label.getText() + '\n' + result);

                    result = input.readLine();
                    label.setText(label.getText() + '\n' + result);

                    result = input.readLine();
                    label.setText(label.getText() + '\n' + result);

                } catch (IOException e) {
                    e.printStackTrace();
                }
                textField.clear();
            }
        });

        rootNode.getChildren().addAll(scrollPane, textField, button);

        myStage.show();
    }

    public void stop() throws IOException {
        output.println("stop");

        server.close();
        input.close();
        output.close();
    }
}
