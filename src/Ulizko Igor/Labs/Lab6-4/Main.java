/**
 * Created by anton on 09.05.17.
 */
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.time.LocalDate;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Main {
    public static void main(String[] args) throws IOException {
        URL url = new java.net.URL("http://cbr.ru/currency_base/daily.aspx?date_req=" + LocalDate.now());
        BufferedReader br = new BufferedReader(new InputStreamReader(url.openStream(), "UTF-8"));

        String line;

        while ((line = br.readLine()).indexOf("USD") == -1);
        br.readLine();
        br.readLine();
        line = br.readLine();
        //System.out.println(line);

        Pattern pattern = Pattern.compile("\\d+,\\d+");
        Matcher matcher = pattern.matcher(line);

        while (matcher.find())
            System.out.println(line.substring(matcher.start(), matcher.end()));

        br.close();
    }
}