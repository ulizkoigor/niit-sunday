import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Random;

/**
 * Created by ulizko on 17.08.2017.
 */

public class Server  {
    ServerSocket serverSocket;
    Socket clientSocket;

    BufferedReader input;
    PrintWriter output;

    ArrayList<String> aphorisms = new ArrayList<String>();


    Server() throws IOException {
        BufferedReader br = new BufferedReader(new FileReader("aphorisms.txt"));
        String str;

        while ((str = br.readLine()) != null)
            aphorisms.add(str);
    }



    public void serverStart() throws IOException {
        try {
            serverSocket = new ServerSocket(1234);
            System.out.println(LocalTime.now().format(DateTimeFormatter.ofPattern("HH:mm:ss")) + " The server is running");
        } catch (IOException exc) {
            System.out.println((LocalTime.now().format(DateTimeFormatter.ofPattern("HH:mm:ss")) + " Error connecting to a port 1234"));
            System.exit(-1);
        }

        try {
            System.out.println(LocalTime.now().format(DateTimeFormatter.ofPattern("HH:mm:ss")) + " Waiting for connection");
            clientSocket = serverSocket.accept();
            System.out.println(LocalTime.now().format(DateTimeFormatter.ofPattern("HH:mm:ss")) + " A client is connected");
        } catch (IOException exc) {
            System.out.println((LocalTime.now().format(DateTimeFormatter.ofPattern("HH:mm:ss")) + " Error connecting to a port 1234"));
            System.exit(-1);
        }

        input = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
        output = new PrintWriter(clientSocket.getOutputStream(), true);
    }



    public void FunctionalityOfTheServer() throws IOException {
        String cmd;

        for ( ; ; ) {
            if ((cmd = input.readLine()) != null) {
                if (cmd.equalsIgnoreCase("stop")) {
                    System.out.println(LocalTime.now().format(DateTimeFormatter.ofPattern("HH:mm:ss")) + " Exit");
                    return;
                }
                if (cmd.equalsIgnoreCase("requestDate")) {
                    System.out.println(LocalTime.now().format(DateTimeFormatter.ofPattern("HH:mm:ss")) + " Request date");
                    output.println(String.valueOf(LocalDateTime.now().format(DateTimeFormatter.ofPattern("dd.MM.yyyy  HH:mm:ss"))));
                }
                if (cmd.equalsIgnoreCase("requestAphorism")) {
                    Random random = new Random();
                    System.out.println(LocalTime.now().format(DateTimeFormatter.ofPattern("HH:mm:ss")) + " Request aphorism");
                    output.println(aphorisms.get(random.nextInt(aphorisms.size())));
                }
            }
        }
    }



    public void serverStop() throws IOException {
        input.close();
        output.close();
        serverSocket.close();
        clientSocket.close();

        System.out.println(LocalTime.now().format(DateTimeFormatter.ofPattern("HH:mm:ss")) + " Connection closed");
    }



    public static void main(String[] args) throws IOException {
        Server sv = new Server();

        sv.serverStart();
        sv.FunctionalityOfTheServer();
        sv.serverStop();
    }
}
