import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.FlowPane;
import javafx.stage.Stage;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

/**
 * Created by ulizko on 16.08.2017.
 */

public class Client extends Application  {
    Label label;
    Socket server;
    BufferedReader input;
    PrintWriter output;

    public static void main(String[] args)  {
        launch(args);
    }

    public void init() {
        try {
            server = new Socket("127.0.0.1", 1234);

            input = new BufferedReader(new InputStreamReader(server.getInputStream()));
            output = new PrintWriter(server.getOutputStream(), true);

            System.out.println((LocalTime.now().format(DateTimeFormatter.ofPattern("HH:mm:ss")) + " Connection ok at 172.0.0.1 port 1234"));

        } catch (IOException exc) {
            System.out.println((LocalTime.now().format(DateTimeFormatter.ofPattern("HH:mm:ss")) + " Connection error at 172.0.0.1 port 1234"));
            System.exit(-1);
        }
    }

    public void start(Stage myStage) {
        myStage.setTitle("sunday client");

        FlowPane rootNode = new FlowPane(10,10);

        rootNode.setAlignment(Pos.CENTER);

        Scene myScene = new Scene(rootNode,400,500);

        myStage.setScene(myScene);

        label = new Label("Make a selection");


        ScrollPane scrollPane = new ScrollPane(label);
        scrollPane.setPrefViewportWidth(300);
        scrollPane.setPrefViewportHeight(400);



        Button btnRqstDate = new Button("To request a date");
        Button btnRqstAphorism = new Button("To request the Aphorism");

        btnRqstDate.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                String result;
                output.println("requestDate");
                try {
                    result = input.readLine();
                    label.setText(label.getText() + '\n' + result);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });

        btnRqstAphorism.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                String result;
                output.println("requestAphorism");
                try {
                    result = input.readLine();
                    label.setText(label.getText() + '\n' + result);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });

        rootNode.getChildren().addAll(scrollPane, btnRqstDate, btnRqstAphorism);

        myStage.show();
    }

    public void stop() throws IOException {
        output.println("stop");

        server.close();
        input.close();
        output.close();
    }
}
