package com.company;


import java.io.*;
import java.util.ArrayList;

enum STATES {
    OFF, WAIT, ACCEPT, CHECK, COOK
}

class Automata {
    int cash;
    STATES state;
    ArrayList<String> menu = new ArrayList<>();
    ArrayList<Integer> prices = new ArrayList<>();

    public Automata() {
        state = STATES.OFF;
    }

    public void printMenu() {
        for (int i = 0; i < menu.size(); i++) {
            System.out.println(menu.get(i) + "..." + prices.get(i));
        }
    }

    public void on() {
        String str = "";
        try (BufferedReader menuFile = new BufferedReader(new FileReader("menu.txt"))) {
            while ((str = menuFile.readLine()) != null)
                try {
                    prices.add(Integer.parseInt(str));
                } catch (NumberFormatException e) {
                    menu.add(str);
                }
        } catch (IOException exc) {
            System.out.println("ОШИБКА ВВОДА-ВЫВОДА ИЗ ФАЙЛА МЕНЮ");
        }
        state = STATES.WAIT;
    }

    public void off() {
        state = STATES.OFF;
    }

    public void coin (int coinUser) {
        state = STATES.ACCEPT;
        cash += coinUser;
    }

    public void choice(String usersChoice) {
        state = STATES.CHECK;
        for (int i = 0; i < menu.size(); i++)
            if (menu.get(i).equals(usersChoice))
                check(i);
    }

    public void cancel() {
        cash = 0;
        state = STATES.WAIT;
    }

    public void printState() {
        System.out.println(state);
    }

    private boolean check(int numUsersChoice) {
        if (cash >= prices.get(numUsersChoice)) {
            cook(numUsersChoice);
            return true;
        }
        else return false;
    }

    private void cook (int numUsersChoice) {
        state = STATES.COOK;
        finish(numUsersChoice);
    }

    private void finish(int numUsersChoice) {
        System.out.print("ЗАБЕРИТЕ НАПИТОК. СПАСИБО! ПРИХОДИТЕ К НАМ ЕЩЕ!" + ((cash - prices.get(numUsersChoice) != 0) ? " ВАША СДАЧА: " + (cash - prices.get(numUsersChoice)) + "\n" : "\n" ));
        cash = 0;
        state = STATES.WAIT;
    }
}

public class Main {
    public static void main(String[] args) throws InterruptedException {
        Automata automata = new Automata();
        automata.on();
        automata.printState();
        automata.coin(30);
        automata.printMenu();
        automata.cancel();
        automata.coin(30);
        automata.printState();
        automata.choice("tea");
        automata.printState();
        automata.off();
        automata.printState();
    }
}
